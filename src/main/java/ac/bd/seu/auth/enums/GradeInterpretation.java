/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.bd.seu.auth.enums;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */
public enum GradeInterpretation {
    
    OUTSTANDING(4, "A+"),
    EXCELLENT(3.75, "A"),
    QUITE_EXCELLENT(3.50, "A-"),
    VERY_GOOD(3.25, "B+"),
    GOOD(3.00, "B"),
    QUITE_GOOD(2.75, "B-"),
    ABOVE_AVERAGE(2.50, "C+"),
    AVERAGE(2.25, "C"),
    POOR(2.00, "D"),
    FAIL(0.00, "F"),
    WITHDRAW(0.00, "W"),
    INCOMPLETE(0.00, "I"),
    OTHERS(0.00, "F");
    
    private double gradePoint;
    private String letterGrade;

    private GradeInterpretation() {
    }

    private GradeInterpretation(double gradePoint, String letterGrade) {
        this.gradePoint = gradePoint;
        this.letterGrade = letterGrade;
    }

    public String getLetterGrade() {
        return letterGrade;
    }

    public void setLetterGrade(String letterGrade) {
        this.letterGrade = letterGrade;
    }

    public double getGradePoint() {
        return gradePoint;
    }

    public void setGradePoint(double gradePoint) {
        this.gradePoint = gradePoint;
    }
    
    
    
}
