/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.bd.seu.auth.enums;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */
public enum ReligionType {
    
    ISLAM("Islam"),
    HINDU("Hindu"),
    CHRISTIAN("Christian"),
    BUDDHA("Buddha"),
    OTHERS("Others");
    
    String religionName;

    private ReligionType(String religionName) {
        this.religionName = religionName;
    }

    public String getReligionName() {
        return religionName;
    }
    
}
