/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.bd.seu.auth.enums;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */
public enum District {
    
    DHAKA("Dhaka"),
    GAZIPUR("Gazipur"),
    KUSHTIA("Kushtia"),
    RAJBARI("Rajbari"),
    JESSORE("Jessore"),
    Feni("Feni"),
    SIRAJGONJ("Sirajgonj"),
    BARISAL("Barisal"),
    PIROJPUR("Pirojpur"),
    NARSHINGDI("Narshingdi"),
    RAJSHAHI("Rajshahi"),
    JHENAIDAH("Jhenaidah"),
    PABNA("Pabna"),
    COMILLA("Comilla"),
    MYMENSINGH("Mymensingh"),
    SATKHIRA("Satkhira"),
    TANGAIL("Tangail"),
    BOGRA("Bogra"),
    CHANDPUR("Chandpur"),
    NAOGAON("Naogaon"),
    NOAKHALI("Noakhali"),
    MUNSHIGONJ("Munshigonj"),
    RANGPUR("Rangpur"),
    BBARIA("BBaria"),
    MAGURA("Magura"),
    BAGERHAT("Bagerhat"),
    MANIKGANJ("Manikgonj"),
    CHITTAGONG("Chittagong"),
    SHARIATPUR("Shariatpur"),
    JAMALPUR("Jamalpur"),
    FARIDPUR("Faridpur"),
    NATORE("Natore"),
    JOYPURHAT("Joypurhat"),
    NILPHAMARI("Nilphamari"),
    PATUAKHALI("Patuakhali"),
    KISHORGONJ("Kishorgonj"),
    NARAYANGONJ("Narayangonj"),
    GOPALGONJ("Gopalgonj"),
    SHERPUR("Sherpur"),
    BORGUNA("Borguna"),
    SYLHET("Sylhet"),
    CHUADANGA("Chuadanga"),
    DINAJPUR("Dinajpur"),
    KHULNA("Khulna"),
    COXS_BAZAR("Coxs Bazar"),
    BHOLA("Bhola"),
    KURIGRAM("Kurigram"),
    NETROKONA("Netrokona"),
    MEHERPUR("Meherpur"),
    THAKURGAON("Thakurgaon"),
    MADARIPUR("Madaripur"),
    NARAIL("Narail"),
    CHAPAINABABGANJ("Chapainawabganj"),
    LALMONIRHAT("Lalmonirhat"),
    HOBIGONJ("Hobigonj"),
    MOULAVIBAZAR("MoulaviBazar"),
    JHALOKHATI("Jhalokathi"),
    GAIBANDHA("Gaibandha"),
    PANCHAGHAR("Panchagahr"),
    RANGAMATI("Rangamati"),
    SUNAMGONJ("Sunamgonj"),
    BANDARBAN("Bandarban"),
    KHAGRACHARI("Khagrachari");
    
    String district;

    private District(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return district;
    }
    
    
}
