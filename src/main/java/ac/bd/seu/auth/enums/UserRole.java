/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.bd.seu.auth.enums;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */

@NoArgsConstructor
public enum UserRole {
    STUDENT("Student"),
    FACULTY("Faculty"),
    CO_ORDINATOR("Program Coordinator"),
    CHAIRMAN("Chairman"),  
    ADMISSION("Admission"),
    ACCOUNTS("Accounts"),
    EXAM("Exam"),
    LIBRARY("Library"),
    PO("Program Officer"),
    DEAN("Dean"),
    REGISTRAR("Registrar"),
    REGISTRATION("Registration"),
    HR("Human Resource"),
    DCS("Career Service"),
    PROVC("Pro VC"),
    VC("VC");
    
    private String roleName;

    UserRole(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
