/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.bd.seu.auth.enums;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */
public enum FacultyAvailability {
    
    AVAILABLE(0, "Available"),
    ON_LEAVE(1, "On-Leave"),
    NOT_AVAILABLE(2, "Not Available");
    
    
    int facultyAvailabilityId;
    String facultyAvailability;

    private FacultyAvailability(int facultyAvailabilityId, String facultyAvailability) {
        this.facultyAvailabilityId = facultyAvailabilityId;
        this.facultyAvailability = facultyAvailability;
    }

    public int getFacultyAvailabilityId() {
        return facultyAvailabilityId;
    }

    public void setFacultyAvailabilityId(int facultyAvailabilityId) {
        this.facultyAvailabilityId = facultyAvailabilityId;
    }

    public String getFacultyAvailability() {
        return facultyAvailability;
    }

    public void setFacultyAvailability(String facultyAvailability) {
        this.facultyAvailability = facultyAvailability;
    }

}
