package ac.bd.seu.auth.service;

/**
 * <h1>SecurityService Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 23:53
 */

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
