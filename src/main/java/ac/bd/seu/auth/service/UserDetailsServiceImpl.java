package ac.bd.seu.auth.service;

import ac.bd.seu.auth.enums.UserRole;
import ac.bd.seu.auth.model.Employee;
import ac.bd.seu.auth.model.Student;
import ac.bd.seu.auth.repository.EmployeeRepository;
import ac.bd.seu.auth.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * <h1>UserDetailsServiceImpl Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 23:56
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        Student student = studentRepository.findOne(username);
        Employee employee = null;
        if(student == null) {
            employee = employeeRepository.findOne(username);
            for (UserRole userRole : employee.getUserRoles()){
                grantedAuthorities.add(new SimpleGrantedAuthority(userRole.getRoleName()));
            }
        } else if(employee == null) {
            grantedAuthorities.add(new SimpleGrantedAuthority("Student"));
        }

        return new org.springframework.security.core.userdetails.User(student.getId(), student.getPassword(), grantedAuthorities);
    }
}
