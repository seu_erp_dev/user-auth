package ac.bd.seu.auth.model;

import ac.bd.seu.auth.enums.District;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * <h1>WorkingExperience Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:21
 */

@Data
@NoArgsConstructor
//@Entity
public class WorkingExperience implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    private String instituteName;
    private String instituteType;
    @Enumerated(EnumType.STRING)
    private District instituteLocation;
    private String position;
    private String department;
    private String responsibility;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar joiningYear;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar endingYear;
}
