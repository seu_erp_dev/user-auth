package ac.bd.seu.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * <h1>Reference Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 00:59
 */

@Data
@NoArgsConstructor
//@Entity
public class Reference implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    private String referenceName;
    private String referenceDesignation;
    private String referenceDepartment;
    private String referenceInstitute;
    private String referenceMobileNumber;
    private String referenceDetails;
}
