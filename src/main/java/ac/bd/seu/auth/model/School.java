package ac.bd.seu.auth.model;

import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>School Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:11
 */

@Data
@NoArgsConstructor
@Entity
public class School implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    @NotBlank   //The string is not null and the trimmed length is greater than zero.
    private String schoolName;
    @OneToMany
    @JoinColumn(name = "school_id")
    private List<Department> departments = new ArrayList<>();

    public School(String schoolName) {
        this.schoolName = schoolName;
    }
}
