package ac.bd.seu.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Department Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:11
 */
@Data
@NoArgsConstructor
@Entity
public class Department implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    @NotBlank   //The string is not null and the trimmed length is greater than zero.
    @Size(min = 5, max = 255)
    private String departmentName;
    @NotNull    //The CharSequence, Collection, Map or Array object is not null, but can be empty.
    @ManyToOne
    private School school;
    @OneToMany
    @JoinColumn(name = "department_id")
    private List<Program> programs = new ArrayList<>();

    public Department(String departmentName, School school) {
        this.departmentName = departmentName;
        this.school = school;
    }
}
