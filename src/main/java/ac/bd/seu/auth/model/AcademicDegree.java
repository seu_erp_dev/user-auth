package ac.bd.seu.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * <h1>AcademicDegree Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:31
 */

@Data
@NoArgsConstructor
//@Entity
public class AcademicDegree implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    //private AcademicDegreeType academicDegreeType;
    private String instituteName;
    private String board;
    private String department;  //Department or Subject
    private String result;       //Class/ Grade/ Numerical Value Only
    private String gradeOutOf;  //3.69 out of 4.00
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar passingYear;
}
