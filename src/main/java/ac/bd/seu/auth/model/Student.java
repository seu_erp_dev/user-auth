package ac.bd.seu.auth.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <h1>Student Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:01
 */

@Data
@NoArgsConstructor
@Entity
public class Student implements Serializable {
    @Id         //Primary key
    @NotNull    //The CharSequence, Collection, Map or Array object is not null, but can be empty.
    @Size(min = 13, max = 13)
    private String id;
    private String name;
    @NotNull    //The CharSequence, Collection, Map or Array object is not null, but can be empty.
    @ManyToOne
    private Program program;
    private int batch;
    //private Major major;
    //@Convert(converter = StringCryptoConverter.class)
    private String password;
    @Transient
    private String confirmPassword;
/*
    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<UserRole> userRoles;*/
/*
    private String fatherName;
    private String motherName;
    private String permanentAddress;
    private String presentAddress;
    private String contactNumber;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dateOfBirth;
    @Enumerated(EnumType.STRING)
    private BloodGroup blood;
    @Enumerated(EnumType.STRING)
    private ReligionType religion;
    @Enumerated(EnumType.ORDINAL)
    private MaritalStatus maritalStatus;
    private String occupation;
    private String studentMonthlyIncome;
    @Enumerated(EnumType.ORDINAL)
    private Citizen citizen;
    private String fatherContactNumber;
    private String fatherOccupation;
    @Enumerated(EnumType.ORDINAL)
    private District district;
    private String sourceOfApplication;

    private String guardianName;
    private String guardianEmail;
    private String guardianContact;
    private String guardianPhone;
    private String guardianRelation;
    private String guardianOccupation;
    private String guardianMonthlyIncome;
    private String guardianQualification;
    private String guardianPresentAddress;
    private String guardianPermanentAddress;

    @OneToMany
    @JoinColumn(name = "academicDegree_id")
    private List<AcademicDegree> academicDegrees = new ArrayList<>();

    private String careerObjective;
    @OneToMany
    @JoinColumn(name = "workingExperience_id")
    private List<WorkingExperience> workingExperiences = new ArrayList<>();
    @OneToMany
    @JoinColumn(name = "projectExperience_id")
    private List<ProjectExperience> projectExperiences = new ArrayList<>();
    @OneToMany
    @JoinColumn(name = "reference_id")
    private List<Reference> references = new ArrayList<>();
    @ElementCollection
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "language_id")
    private List<Language> languageSkills = new ArrayList<>();
    @ElementCollection
    @JoinColumn(name = "skill_id")
    private List<String> skills = new ArrayList<>();
    @ElementCollection
    @JoinColumn(name = "award_id")
    private List<String> awards = new ArrayList<>();
    @ElementCollection
    @JoinColumn(name = "coCurriculumActivity_id")
    private List<String> coCurriculumActivities = new ArrayList<>();
    @ElementCollection
    @JoinColumn(name = "interest_id")
    private List<String> interests = new ArrayList<>();

    private String email;
    private String website;
    private String facebookUrl;
    private String twitterUrl;
    private String linkedinUrl;
    private String githubUrl;
    private String pictureUrl;

    @OneToOne
    private SecurityQuestion securityQuestion;
*/
}
