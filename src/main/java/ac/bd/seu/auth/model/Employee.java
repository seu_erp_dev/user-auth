package ac.bd.seu.auth.model;

import ac.bd.seu.auth.enums.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * <h1>Employee Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 01:21
 */
@Entity
@Data
@NoArgsConstructor
public class Employee implements Serializable {
    @Id         //Primary key
    @NotNull    //The CharSequence, Collection, Map or Array object is not null, but can be empty.
    @Size(min = 3, max = 10)
    private String username;
    private String password;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<UserRole> userRoles;

    /*
    private String name;
    private String contactNumber;
    private String fatherName;
    private String motherName;
    private String permanentAddress;
    private String presentAddress;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dateOfBirth;
    @Enumerated(EnumType.STRING)
    private BloodGroup blood;
    @Enumerated(EnumType.STRING)
    private ReligionType religion;
    @Enumerated(EnumType.STRING)
    private Citizen citizen;
    @Enumerated(EnumType.STRING)
    private MaritalStatus maritalStatus;
    @OneToMany
    @JoinColumn(name = "workingExperience_id")
    private List<WorkingExperience> workingExperiences;
    @OneToMany
    @JoinColumn(name = "projectExperience_id")
    private List<ProjectExperience> projectExperiences;
    @OneToMany
    @JoinColumn(name = "reference_id")
    private List<Reference> references;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar joinedDate;
    @OneToOne
    private SecurityQuestion securityQuestion;
    private String pictureUrl;

    private String email;
    private String website;
    private String facebookUrl;
    private String twitterUrl;
    private String linkedinUrl;
    private String githubUrl;
    private String studentPictureUrl;
*/
}
