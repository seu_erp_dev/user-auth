package ac.bd.seu.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <h1>Program Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 23 Oct 2017 19:12
 */
@Data
@NoArgsConstructor
@Entity
public class Program implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    @NotBlank   //The string is not null and the trimmed length is greater than zero.
    @Size(min = 5, max = 255)
    private String programName;
    @NotNull    //The CharSequence, Collection, Map or Array object is not null, but can be empty.
    @ManyToOne
    private Department department;

    public Program(String programName, Department department) {
        this.programName = programName;
        this.department = department;
    }
}
