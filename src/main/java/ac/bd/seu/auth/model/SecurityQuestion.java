package ac.bd.seu.auth.model;

import ac.bd.seu.auth.enums.SecurityQuestionEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * <h1>SecurityQuestion Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 01:22
 */

@Data
@NoArgsConstructor
//@Entity
public class SecurityQuestion implements Serializable {
    @Id         //Primary key
    @GeneratedValue(strategy = GenerationType.AUTO)     //Auto Generated
    private int id;
    @Enumerated(EnumType.STRING)
    private SecurityQuestionEnum securityQuestionEnum;
    private String securityAnswer;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar securityQuestionAddedTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar securityQuestionLastUpdateTime;
}
