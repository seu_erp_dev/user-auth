package ac.bd.seu.auth.repository;

import ac.bd.seu.auth.model.Student;
import org.springframework.data.repository.CrudRepository;

/**
 * <h1>UserRepository Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 24 Oct 2017 23:52
 */

public interface StudentRepository extends CrudRepository<Student, String> {
}