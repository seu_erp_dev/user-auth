package ac.bd.seu.auth.validator;

import ac.bd.seu.auth.model.Student;
import ac.bd.seu.auth.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * <h1>UserValidator Short Description</h1>
 * Briefly describe why you created this file
 * <p>
 * Giving proper comments in your program makes it more user friendly and it is assumed as a high quality code. Describe details of this file.
 *
 * @author Asaduzzaman Noor
 * @version 1.0
 * @since 25 Oct 2017 00:00
 */

@Component
public class UserValidator implements Validator {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return Student.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Student student = (Student) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "NotEmpty");
        if (student.getId().length() != 13) {
            errors.rejectValue("id", "Size.userForm.id");
        }
        if (studentRepository.findOne(student.getId()) != null) {
            errors.rejectValue("id", "Duplicate.userForm.id");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (student.getPassword().length() < 8 || student.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        if (!student.getConfirmPassword().equals(student.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
        }
    }
}

